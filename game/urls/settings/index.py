from django.urls import path,include
from game.views.settings.getinfo import getinfo
from game.views.settings.aclogin import aclogin
from game.views.settings.acregister import acregister
from game.views.settings.aclogout import aclogout

urlpatterns = [
     path('acwing/',include('game.urls.settings.acwing.index')),
     path('getinfo/',getinfo,name='settings_getinfo'),
     path('aclogin/',aclogin,name='settings_aclogin'),
     path('acregister/',acregister,name='settings_acregister'),
     path('aclogout/',aclogout,name='settings_aclogout'),
        ]
