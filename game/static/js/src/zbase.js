export class AcGame{

    constructor(id,AcWingOS){
        this.id = id;
        this.AcWingOS = AcWingOS;
        this.$ac_game = $("#" + this.id);
        this.settings = new Settings(this);
        this.menu = new AcGameMenu(this);
        this.menu.hide();
        this.playground = new AcGamePlayground(this);   
        this.playground.hide();
    }

    start(){

    }
    
}

