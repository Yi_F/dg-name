class AcGamePlayer  extends AcGameObject {
    
    constructor(game_map, x, y, radius ,speed, is_me,color = "rgb(255,255,255)"){
        super();
        this.game_map = game_map;
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.speed = speed;
        this.ctx = this.game_map.ctx;
        this.is_me = is_me;
        this.move_length = 0;
        this.eps = 0.1;
        this.vx = 0;
        this.vy = 0;
        this.dx = 0;
        this.dy = 0;
        this.dange_length = 0;
        this.dange_speed = this.speed; //击中后后退的速度
        this.dange_smaller_speed//击中后球半径减少的速度
        this.friction = 0.9; // 摩擦力系数
        this.shoted_balls = [];
        this.color = color;
        this.destroy_eps= this.radius/10
        this.self_cur_skill;

        if( this.is_me ){
            console.log("settings="+this.settings);
            let settings = this.game_map.playground.root.settings;
            this.photo = settings.photo;
            this.username = settings.username;

			this.img = new Image();
			this.img.src = settings.photo;

        }

    }

    start(){
        if(this.is_me){
            this.add_listening_event();
        }else{
            this.random_moveTo();
        }
        this.add_listening_event();
    }

    random_moveTo(){
        let rx = Math.random() * this.game_map.width;
        let ry = Math.random() * this.game_map.height;
        this.moveTo(rx,ry);
    }



   add_listening_event(){
        let outer = this;
        if( this.is_me){
            this.game_map.$canvas.mousedown(function(e){
               // 右键点击-
                if(e.buttons === 2){
                    let rectObject = outer.game_map.ctx.canvas.getBoundingClientRect();
                    outer.game_map.self_player.moveTo(e.clientX-rectObject.left,e.clientY - rectObject.top);

                }else if(e.buttons === 1){ // 左键点击
                   // console.log("左键点击");

                    if(outer.self_cur_skill === "FireBall"){
                       // console.log("set FireBall");
                        let ro = outer.game_map.ctx.canvas.getBoundingClientRect();
                        outer.shot_Fireball(e.clientX - ro.left, e.clientY - ro.top);
                        outer.self_cur_skill = "";
                    }
                }

            });

            this.game_map.$canvas.contextmenu(function(e){
                e.preventDefault();
            });

            $(window).keydown(function(e){
               if(e.which ===  81){
                    outer.self_cur_skill = "FireBall";
               }
            });
        }
    }

    random_shoted(){
        let n = this.game_map.players.length;
        let one = this.game_map.players[Math.floor(Math.random() * n)];
        this.shot_Fireball(one.x,one .y);
    }

    shot_Fireball(tx,ty){

        this.shot_ball(tx,ty,this.radius / 3 , this.speed * 2,this.game_map.height,this.radius / 5,this.color);

    }

    shot_ball(tx,ty,radius,speed,mov_len,dange,color){
        let x = this.x;
        let y = this.y;
        let  t = Math.atan2(ty-y,tx-x);
        let vx = Math.cos(t);
        let vy = Math.sin(t);
        let ball;
        this.shoted_balls.push(ball = new AcGameFireball(this.game_map,this,x,y,radius,vx,vy,speed,mov_len,dange,color));
        this.game_map.balls.push(ball);
    }


    update(){
        if(this.radius < this.destroy_eps){
            this.destroy();
            return ;
        }
        if(this.dange_length < this.eps){
            this.dange_length = 0;
            this.dange_speed = 0;
            this.dx = 0;
            this.dy = 0;
            this.dange_smaller_speed = 0;
            if(this.move_length < this.eps){
                this.move_length = 0;
                this.vx = 0;
                this.vy =0;
                if(! this.is_me){
                    this.random_moveTo();
                }
            }else{
                let movlen = this.speed * this.timedetla / 1000;
                this.x += this.vx * movlen;
                this.y += this.vy * movlen;
                this.move_length -= movlen ;

            }
            if(! this.is_me && Math.random() * 180 <= 1){

                this.random_shoted();

            }
        }else{

            this.vy = 0;
            this.vx = 0;
            this.move_length = 0;
            let movlen = this.dange_speed * this.timedetla / 1000;
            this.x += this.dx * movlen;
            this.y += this.dy * movlen;
            this.dange_length -= movlen;
            //console.log(this.dange_length);
            //this.radius -= this.dange_smaller_speed;
            //this.dange_speed *= this.friction;
        }
        this.render();

    }

    moveTo(tx,ty){
        let t = Math.atan2(ty-this.y,tx-this.x);
        this.move_length = this.getDist(tx-this.x,ty-this.y);
        this.vx = Math.cos(t);
        this.vy = Math.sin(t);
    }

    attacked(dx,dy,dange_speed,dange,dange_length){
        for(let i = 0;i< 20 + Math.random()*10 ;i++){
            let t = Math.PI * 2 * Math.random();
            let vx = Math.cos(t);
            let vy = Math.sin(t);
            let tmp = new Particle(this.game_map,this.x,this.y,this.radius * 0.1 * Math.random(),this.color,vx,vy,this.speed * 5 ,this.radius * 5 * Math.random());

        }
        console.log("hit"+this);
        this.dx = dx;
        this.dy = dy;
        this.dange_speed = dange_speed;
        this.dange_length = dange_length;
        this.dange_smaller_speed = dange / (dange_length/dange_speed/this.timedetla* 1000);
        this.radius -= dange;
    }

    getDist(tx,ty){

        return Math.sqrt(tx * tx + ty * ty);

    }

    on_destroy(){


        for(let i=0;i<this.game_map.length;i++){
            if(this.game_map[i] === this){
                this.game_map.players.splice(i,1);
                break;
            }

        }

    }


    render(){

       if(this.is_me && this.img){
		   this.ctx.save();
		   this.ctx.beginPath();
		   this.ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
		   this.ctx.stroke();
		   this.ctx.clip();
		   this.ctx.drawImage(this.img, this.x - this.radius, this.y - this.radius, this.radius * 2, this.radius * 2); 
		   this.ctx.restore();
            

       }else{

           this.ctx.beginPath();
           this.ctx.fillStyle = this.color;
           this.ctx.arc(this.x,this.y,this.radius,0,2*Math.PI,false);
           this.ctx.fill();
       }
    }

}
