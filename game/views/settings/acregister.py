from django.http import JsonResponse
from django.contrib.auth.models import User
from game.models.settings.player.player import Player
from django.contrib.auth import authenticate, login
def acregister(req):


    data = req.GET
    print(data.get('username'))
    username = data.get('username','').strip();
    password = data.get('password','').strip()
    comfirm_pd = data.get('comfirm_password','').strip();
    default_image ="https://cdn.acwing.com/media/user/profile/photo/171022_lg_60913a6bc5.jpg"
        
    if not username or not password or not comfirm_pd:
        return JsonResponse({
            'result': '用户名和密码不能为空',
            });

    if password != comfirm_pd :
        return JsonResponse({
                
            'result': '两个密码不正确',
            })

    if( User.objects.filter(username=username).exists()):
        return JsonResponse({
                'result': '用户名已经存在',
            });

    user = User(username=username);
    user.set_password(password);
    user.save();
    Player.objects.create(user=user,photo=default_image);
    login(req,user)
    return JsonResponse({

        'result': 'success',

        });

