from django.http import JsonResponse
import random
import urllib
from django.core.cache import cache
def apply_code(resp):
    appid = '2156'
    redirect_uri = urllib.parse.quote("https://app2156.acapp.acwing.com.cn/settings/acwing/web/receive_code/")
    scope='userinfo'
    state=get_state(12)
    apply_url = 'https://www.acwing.com/third_party/api/oauth2/web/authorize/'
    print(state);
    cache.set(state,0,7200)

    return JsonResponse({

        'result': 'success',
        'apply_code_url': apply_url + '?appid=%s&redirect_uri=%s&scope=%s&state=%s' % (appid, redirect_uri,scope, state),
        });




def get_state(n):
    sb = ''
    for i in range(n):
        sb += str(random.randint(1,9))
    return sb
