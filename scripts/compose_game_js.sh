#! /bin/bash

JS_ROOT_PATH=/home/cwj2001/acapp/game/static/js/
JS_SRC_PATH=${JS_ROOT_PATH}src/
JS_DIST_PATH=${JS_ROOT_PATH}dist/

find ${JS_SRC_PATH}   -type f -name "*.js" | sort | xargs cat > ${JS_DIST_PATH}game.js

echo yes | python3 /home/cwj2001/acapp/manage.py collectstatic
