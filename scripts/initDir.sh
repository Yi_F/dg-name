#! /bin/bash

root_dir=/home/cwj2001/acapp

cur_dir=${root_dir}/game

rm -f urls.py models.py views.py

fn="__init__.py"
for i in urls models views templates
do
    t_dir=${cur_dir}/${i}
    mkdir -p ${t_dir}
    touch ${t_dir}/${fn}
    for j in 'menu' 'playground' 'settings'
    do
        mkdir -p ${t_dir}/${j}
        touch ${t_dir}/${j}/${fn}
    done
done


mkdir -p ${cur_dir}/static
for i in 'css' 'js' 'image' 'audio'
do
    mkdir -p ${cur_dir}/static/${i}
    t_dir=${cur_dir}/static/${i}
	mkdir -p ${t_dir}/menu  ${t_dir}/playground  ${t_dir}/settings
done

