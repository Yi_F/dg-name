class AcGameMenu{

    constructor(root){
       
        this.root = root;

        this.$menu = $(`
<div class="ac-game-menu">
    <div class="ac-game-menu-field">
        <div class="ac-game-menu-field-item ac-game-menu-field-item-single-mode">
               单人模式
        </div>
        <br/>
        <div class="ac-game-menu-field-item ac-game-menu-field-item-multi-mode">
            多人模式
        </div>
        <br/>
        <div class="ac-game-menu-field-item ac-game-menu-field-item-settings">
            退出
        </div>
        <br/>
    </div>
</div>
            `);
        this.root.$ac_game.append(this.$menu)
        this.$single_mode = this.$menu.find("."+"ac-game-menu-field-item-single-mode");
        this.$multi_mode = this.$menu.find("."+"ac-game-menu-field-item-multi-mode");
        this.$settings = this.$menu.find("."+"ac-game-menu-field-item-settings");
        

        this.start();

    }


    start(){

        this.add_listening_event();

    }

    add_listening_event(){
        let outer = this;
        console.log(this.$single_mode);
        this.$single_mode.click(function (){
            console.log(outer.root);
            outer.root.playground.show();
            outer.hide();
        })

        this.$multi_mode.click(function (){
            console.log("Hell");
        })

        this.add_listening_settings(); 

    }

    add_listening_settings(){
    let outer = this;

     this.$settings.click(function (){
         console.log(outer.root);
            outer.root.settings.logout_to_remote();            
        })

    }


    show(){
        this.$menu.show();

    }

    hide(){
        this.$menu.hide();
    }

}
let AC_GAME_OBJECTS = [];

class AcGameObject{

    constructor(){

        AC_GAME_OBJECTS.push(this);
        this.timedetla = 0;
        this.is_called_start = false;
    }

    start(){



    }


    update(){



    }

    on_destroy(){


    }

    destroy(){
        this.on_destroy();

        for(let i=0;i < AC_GAME_OBJECTS.length;i++){
            if(AC_GAME_OBJECTS[i] === this){
                AC_GAME_OBJECTS.splice(i,1);
            }

        }

    }

}

let last_time = 0;

let AC_GAME_ANIMATION = function (timestamp) {
    
    for(let i=0;i<AC_GAME_OBJECTS.length;i++){
        let x = AC_GAME_OBJECTS[i];
        if(! x.is_called_start){
            x.start();
            x.is_called_start = true;
        }else {
            x.timedetla = timestamp - last_time;

            x.update();
        }

    }
    last_time = timestamp;
    requestAnimationFrame(AC_GAME_ANIMATION);

}
requestAnimationFrame(AC_GAME_ANIMATION);



class AcGameFireball extends AcGameObject{

    constructor(game_map,player,x,y,radius,vx,vy,speed,move_len,dange,color){
        super();
        this.game_map = game_map;
        this.ctx = this.game_map.ctx;
        this.player = player;
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.vx = vx;
        this.vy = vy;
        this.move_length = move_len;
        this.dange = dange;
        this.speed = speed;
        this.color = color;
        this.eps = 0.1;
    }


    start(){


    }

    update(){
        if(this.move_length < this.eps){
            this.destroy();
            let balls = this.game_map.balls;
            for(let i=0;i<this.game_map.balls.length;i++){
                if(this === balls[i]){
                    balls.splice(i,1);
                    break;
                }
            }
            return  ;
        }else{
            let t = this.speed * this.timedetla / 1000;
            this.x += t * this.vx;
            this.y += t * this.vy;
            this.move_length -= t;
        }

        this.render();
    }

    destroy(){
        let balls = this.player.shoted_balls;
        for(let i = 0;i < balls.length;i++){

            if(balls[i] === this){
                balls.splice(i,1);
                break;
            }

        }
        super.destroy();

    }

    attackPlayer(player){

        let t = Math.atan2(player.y-this.y,player.x-this.x);
        let dx = Math.cos(t);
        let dy = Math.sin(t);

       player.attacked(dx,dy,this.speed * 0.75,this.dange,this.dange/player.radius * this.game_map.height / 2);
       // console.log(this.game_map.height);
        //player.attacked(dx,dy,this.speed * 0.75,this.dange,this.game_map.height/1000);


    }

    

    render(){
        
        this.ctx.beginPath();
        this.ctx.fillStyle = this.color;
        this.ctx.arc(this.x,this.y,this.radius,0,2 * Math.PI,false);
        this.ctx.fill();
    }

}
class AcGameMap extends AcGameObject{
    
    constructor(playground){
        super();
        this.playground = playground;
        this.$canvas = $(`<canvas></canvas>`);
        this.ctx = this.$canvas[0].getContext("2d");
        this.playground.$playground.append(this.$canvas);
        this.width = this.playground.width;
        this.height = this.playground.height;
        this.$canvas[0].width = this.width;
        this.$canvas[0].height = this.height;
        this.players = [];
        console.log("---");;
        this.self_player = new AcGamePlayer(this,this.width / 2,this.height / 2 ,this.height * 0.05 , this.height * 0.30 , true);
        this.players.push(this.self_player);
        console.log(this.self_player);
        this.balls = [];
    }

    start(){
        this.create_some_players();
    }

    update(){
        this.render();
        this.check_collision();
    }

    destory(){


    }

    render(){
       this.ctx.fillStyle = "rgb(0,0,0,0.2)";
       this.ctx.fillRect(0,0,this.width,this.height);
    }


    create_some_players(){

        let colors = ["blue","red","green","#e1aa15","#b1c542"];
        let n = 5;
        let one = this.self_player;

        for(let i = 0;i<n;i++){
            this.players.push(new AcGamePlayer(this,one.x,one.y,one.radius,one.speed,false,colors[Math.floor(Math.random() * colors.length)]));

        }

    }

    check_vaild(a){
        if(a.x<0 || a.y < 0 || a.x > this.width || a.y > this.height){
            
            return false;

        }
        else{
            return true;
        }
    }

    check_collision(){
        console.log(this.balls.length);
       const st = new Array(9).fill(false);
       for(let i=0;i<this.balls.length;i++){
           let a = this.balls[i];
           if(st[i] || !this.check_vaild(a)){
               st[i] = true;
               continue;
           }
           let t = false;
           for(let j = i+1;j<this.balls.length;j++){
                let b = this.balls[j];
               if(st[j] || !this.check_vaild(b)){
                    st[j] = true;
                   continue;
               }
                if(this.is_collision(a,b)){
                    st[i] = true;
                    st[j] = true;
                    t = true;
                }

           }
           if(t)  continue;
           
           for(let j = 0;j < this.players.length;j++){
                let b =this.players[j];
                if(a.player != b && this.is_collision(a,b)){
                    a.attackPlayer(b);
                    st[i] = true;
                }
           }

           let nums = [];
           for(let i =0;i<this.balls.length;i++){
                let a = this.balls[i];
                if(st[i])   a.destroy();
                else nums.push(a);
           }
           this.balls = nums;
       }


    }

    is_collision(a,b){

        if(!a.x || !a.y || ! a.radius || ! b.x || !b.y || !b.radius)    return ;
        return this.getDist(a,b)<(a.radius + b.radius);

    }

    getDist(a,b){
        return Math.sqrt((a.x-b.x)*(a.x-b.x) + (a.y - b.y)*(a.y-b.y));
    }


}
class Particle extends AcGameObject{

    constructor(game_map,x,y,radius,color,vx,vy,speed,move_length){
        super();
        this.game_map = game_map;
        this.ctx = this.game_map.ctx;
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.color = color;
        this.vx = vx;
        this.vy = vy;
        this.speed = speed;
        this.friction = 0.9;
        this.move_length = move_length;
        this.eps = 0.1;
    }

    start(){
        


    }

    update(){
        if(this.move_length < this.eps || this.speed < this.eps){
            this.destroy();
            return ;
        }
    
        let t = this.speed * this.timedetla / 1000;
        this.x += t * this.vx;
        this.y += t*this.vy;
        this.speed *= this.friction;
        this.move_length -= t;
        this.render();
    }

    render(){
        this.ctx.beginPath();
        this.ctx.fillStyle = this.color;
        this.ctx.arc(this.x,this.y,this.radius,0,2*Math.PI,false);
        this.ctx.fill();
    }

    on_destroy(){

    }


}
class AcGamePlayer  extends AcGameObject {
    
    constructor(game_map, x, y, radius ,speed, is_me,color = "rgb(255,255,255)"){
        super();
        this.game_map = game_map;
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.speed = speed;
        this.ctx = this.game_map.ctx;
        this.is_me = is_me;
        this.move_length = 0;
        this.eps = 0.1;
        this.vx = 0;
        this.vy = 0;
        this.dx = 0;
        this.dy = 0;
        this.dange_length = 0;
        this.dange_speed = this.speed; //击中后后退的速度
        this.dange_smaller_speed//击中后球半径减少的速度
        this.friction = 0.9; // 摩擦力系数
        this.shoted_balls = [];
        this.color = color;
        this.destroy_eps= this.radius/10
        this.self_cur_skill;

        if( this.is_me ){
            console.log("settings="+this.settings);
            let settings = this.game_map.playground.root.settings;
            this.photo = settings.photo;
            this.username = settings.username;

			this.img = new Image();
			this.img.src = settings.photo;

        }

    }

    start(){
        if(this.is_me){
            this.add_listening_event();
        }else{
            this.random_moveTo();
        }
        this.add_listening_event();
    }

    random_moveTo(){
        let rx = Math.random() * this.game_map.width;
        let ry = Math.random() * this.game_map.height;
        this.moveTo(rx,ry);
    }



   add_listening_event(){
        let outer = this;
        if( this.is_me){
            this.game_map.$canvas.mousedown(function(e){
               // 右键点击-
                if(e.buttons === 2){
                    let rectObject = outer.game_map.ctx.canvas.getBoundingClientRect();
                    outer.game_map.self_player.moveTo(e.clientX-rectObject.left,e.clientY - rectObject.top);

                }else if(e.buttons === 1){ // 左键点击
                   // console.log("左键点击");

                    if(outer.self_cur_skill === "FireBall"){
                       // console.log("set FireBall");
                        let ro = outer.game_map.ctx.canvas.getBoundingClientRect();
                        outer.shot_Fireball(e.clientX - ro.left, e.clientY - ro.top);
                        outer.self_cur_skill = "";
                    }
                }

            });

            this.game_map.$canvas.contextmenu(function(e){
                e.preventDefault();
            });

            $(window).keydown(function(e){
               if(e.which ===  81){
                    outer.self_cur_skill = "FireBall";
               }
            });
        }
    }

    random_shoted(){
        let n = this.game_map.players.length;
        let one = this.game_map.players[Math.floor(Math.random() * n)];
        this.shot_Fireball(one.x,one .y);
    }

    shot_Fireball(tx,ty){

        this.shot_ball(tx,ty,this.radius / 3 , this.speed * 2,this.game_map.height,this.radius / 5,this.color);

    }

    shot_ball(tx,ty,radius,speed,mov_len,dange,color){
        let x = this.x;
        let y = this.y;
        let  t = Math.atan2(ty-y,tx-x);
        let vx = Math.cos(t);
        let vy = Math.sin(t);
        let ball;
        this.shoted_balls.push(ball = new AcGameFireball(this.game_map,this,x,y,radius,vx,vy,speed,mov_len,dange,color));
        this.game_map.balls.push(ball);
    }


    update(){
        if(this.radius < this.destroy_eps){
            this.destroy();
            return ;
        }
        if(this.dange_length < this.eps){
            this.dange_length = 0;
            this.dange_speed = 0;
            this.dx = 0;
            this.dy = 0;
            this.dange_smaller_speed = 0;
            if(this.move_length < this.eps){
                this.move_length = 0;
                this.vx = 0;
                this.vy =0;
                if(! this.is_me){
                    this.random_moveTo();
                }
            }else{
                let movlen = this.speed * this.timedetla / 1000;
                this.x += this.vx * movlen;
                this.y += this.vy * movlen;
                this.move_length -= movlen ;

            }
            if(! this.is_me && Math.random() * 180 <= 1){

                this.random_shoted();

            }
        }else{

            this.vy = 0;
            this.vx = 0;
            this.move_length = 0;
            let movlen = this.dange_speed * this.timedetla / 1000;
            this.x += this.dx * movlen;
            this.y += this.dy * movlen;
            this.dange_length -= movlen;
            //console.log(this.dange_length);
            //this.radius -= this.dange_smaller_speed;
            //this.dange_speed *= this.friction;
        }
        this.render();

    }

    moveTo(tx,ty){
        let t = Math.atan2(ty-this.y,tx-this.x);
        this.move_length = this.getDist(tx-this.x,ty-this.y);
        this.vx = Math.cos(t);
        this.vy = Math.sin(t);
    }

    attacked(dx,dy,dange_speed,dange,dange_length){
        for(let i = 0;i< 20 + Math.random()*10 ;i++){
            let t = Math.PI * 2 * Math.random();
            let vx = Math.cos(t);
            let vy = Math.sin(t);
            let tmp = new Particle(this.game_map,this.x,this.y,this.radius * 0.1 * Math.random(),this.color,vx,vy,this.speed * 5 ,this.radius * 5 * Math.random());

        }
        console.log("hit"+this);
        this.dx = dx;
        this.dy = dy;
        this.dange_speed = dange_speed;
        this.dange_length = dange_length;
        this.dange_smaller_speed = dange / (dange_length/dange_speed/this.timedetla* 1000);
        this.radius -= dange;
    }

    getDist(tx,ty){

        return Math.sqrt(tx * tx + ty * ty);

    }

    on_destroy(){


        for(let i=0;i<this.game_map.length;i++){
            if(this.game_map[i] === this){
                this.game_map.players.splice(i,1);
                break;
            }

        }

    }


    render(){

       if(this.is_me && this.img){
		   this.ctx.save();
		   this.ctx.beginPath();
		   this.ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
		   this.ctx.stroke();
		   this.ctx.clip();
		   this.ctx.drawImage(this.img, this.x - this.radius, this.y - this.radius, this.radius * 2, this.radius * 2); 
		   this.ctx.restore();
            

       }else{

           this.ctx.beginPath();
           this.ctx.fillStyle = this.color;
           this.ctx.arc(this.x,this.y,this.radius,0,2*Math.PI,false);
           this.ctx.fill();
       }
    }

}
class AcGamePlayground{

    constructor(root){
        this.root = root;
        this.$playground = $(`
            <div class="ac-game-playground"></div>
            `);

        this.$playground.hide();
        this.root.$ac_game.append(this.$playground);
        
    }


    start(){
        this.add_listening_event();
    }

    add_listening_event(){


    }


    show(){
        this.width = this.$playground.width();
        this.height = this.$playground.height();
        this.game_map = new AcGameMap(this);
        this.start();
        this.$playground.show();
    }

    hide(){
        this.$playground.hide();
    }

}
class Settings{


    constructor(root){
        this.root = root;
        this.platform;
        if(this.root.AcWingOS) this.platform = "ACAPP";
        else this.platform = "WEB";
        this.photo = "";
        this.username = "";

        this.$settings = $(`
<div class="ac-game-settings">
    <div class="ac-game-settings-login ac-game-settings-field">
        <div class="ac-game-settings-field-title">
        登录
        </div>
        <div class="ac-game-settings-field-username">
            <div class="ac-game-settings-item">
                <input type="text" placeholder="用户名">
            </div>
        </div>
        <br/>
        <div class="ac-game-settings-field-password ">
            <div class="ac-game-settings-item">
                <input type="password" placeholder="密码">
            </div>
        </div>
        <br/>
        <div class="ac-game-settings-field-submit">
            <div class="ac-game-settings-item">
                <button>登录</button>
            </div>
        </div>
        <div>
            <div class="ac-game-settings-field-error-messages">
               错误消息
            </div>
            <div class="ac-game-settings-field-options">
                注册
            </div>
        </div>
        <br/>
        <div class="ac-game-settings-acwing">
            <img width="30" src="https://app165.acapp.acwing.com.cn/static/image/settings/acwing_logo.png">
            <div>
                AcWing一键登录
            </div>
        </div>
    </div>



    <div class="ac-game-settings-register ac-game-settings-field">
        <div class="ac-game-settings-field-title">
        注册
        </div>
        <div class="ac-game-settings-field-username">
            <div class="ac-game-settings-item">
                <input type="text" placeholder="用户名">
            </div>
        </div>
        <br/>
        <div class="ac-game-settings-field-password ">
            <div class="ac-game-settings-item">
                <input type="password" placeholder="密码">
            </div>
        </div>
        <br/>

        <div class="ac-game-settings-field-comfirm-password ">
            <div class="ac-game-settings-item">
                <input type="password" placeholder="确认密码">
            </div>
        </div>

        <br/>
        <div class="ac-game-settings-field-submit">
            <div class="ac-game-settings-item">
                <button>注册</button>
            </div>
        </div>
        <div>
            <div class="ac-game-settings-field-error-messages">
               错误消息
            </div>
            <div class="ac-game-settings-field-options">
                登录
            </div>
        </div>
        <br/>
    </div>


</div>

            `);

        this.root.$ac_game.append(this.$settings);

       this.$login = this.$settings.find(".ac-game-settings-login");
       this.$login_username = this.$login.find(".ac-game-settings-field-username input");
       this.$login_pd = this.$login.find(".ac-game-settings-field-password input");
       this.$login_button = this.$login.find(".ac-game-settings-field-submit button");
       this.$login_error = this.$login.find(".ac-game-settings-field-error-messages");
       this.$login_option = this.$login.find(".ac-game-settings-field-options");
        this.$login_acwing = this.$login.find(".ac-game-settings-acwing");

       this.$register = this.$settings.find(".ac-game-settings-register");
       this.$register_username = this.$register.find(".ac-game-settings-field-username input");
       this.$register_pd = this.$register.find(".ac-game-settings-field-password input");
        this.$register_cd_pd = this.$register.find(".ac-game-settings-field-comfirm-password input");
        this.$register_error = this.$register.find(".ac-game-settings-field-error-messages");
        this.$register_option = this.$register.find(".ac-game-settings-field-options");
        this.$register_button = this.$register.find(".ac-game-settings-field-submit button");



        console.log(this);

        this.start();
    }

    start(){
        this.getinfo();
        this.add_listening_event();
        this.login();
    }

    add_listening_event(){
       
        this.add_listening_register();
        this.add_listening_login();
        this.add_listening_logout();
    }

    add_listening_logout(){
        let outer = this;
    }
    
    logout_to_remote(){
        
        $.ajax({
            
            url:'https://app2156.acapp.acwing.com.cn/settings/aclogout/',
            type: 'GET',
            data:{

            },
            success: function(resp){
                console.log(resp);
                location.reload();
            }

        });

    }
    add_listening_login(){
        let outer = this;
        this.$login_option.click(function(){
           outer.register();
        });

        this.$login_button.click(function(){

            outer.login_to_remote();

        });

        this.$login_acwing.click(function(){

            outer.acwing_web_login();

        });


    }

    login(){
        
        this.$login.show();
        this.$register.hide();

    }

    login_to_remote(){
        let outer = this;
        let username = this.$login_username.val();
        let password = this.$login_pd.val();
        this.$login_error.empty();
        $.ajax({

            'url':'https://app2156.acapp.acwing.com.cn/settings/aclogin/',
            'type': 'GET',
            'data': {
                
                'username': username,
                'password': password,

            },
            'success': function(resp){
                console.log(resp);

                if(resp.result === 'success'){
                    location.reload();
                }else{
                    outer.$login_error.html(resp.result);
                }


            }
            

        });
    }

    acwing_web_login(){


        $.ajax({

            url:'https://app2156.acapp.acwing.com.cn/settings/acwing/web/apply_code/',
            type: 'GET',
            data:{},
            success: function(resp){
                if(resp.result === 'success'){
                    window.location.replace(resp.apply_code_url);
                }

            }

        });


    }

    add_listening_register(){
        let outer = this; 
        this.$register_option.click(function(){
            outer.login();

        });
        this.$register_button.click(function(){
            outer.register_to_remote();

        });

    }

    register(){
        this.$login.hide();
        this.$register.show();
    }

    register_to_remote(){
        let outer = this;
        let username = this.$register_username.val();
        let password = this.$register_pd.val();
        let comfirm_password = this.$register_cd_pd.val();
        this.$register_error.empty();
        
        $.ajax({

            url:'https://app2156.acapp.acwing.com.cn/settings/acregister/',
            type: 'GET',
            data:{
                'username': username,
                'password': password,
                'comfirm_password': comfirm_password,
            },
            success: function(resp){
                if(resp.result === 'success'){
                    
                    location.reload();

                }else{
                    outer.$register_error.html(resp.result);
                }

            }

        });

    }

    getinfo(){
        let outer = this;

        $.ajax({
            url: "https://app2156.acapp.acwing.com.cn/settings/getinfo/",
            type: "GET",
            data:{
                "platform": this.platform,
            },
            success: function(resp){
                console.log(resp);
                if(resp.result === "success"){
                    outer.photo = resp.photo;
                    outer.username = resp.photo;
                    outer.hide();
                    outer.root.menu.show();

                }else{

                    outer.login();

                }
            }

        });


    }


    show(){
        
        this.$settings.show();

    }

    hide(){
        this.$settings.hide();

    }

}
export class AcGame{

    constructor(id,AcWingOS){
        this.id = id;
        this.AcWingOS = AcWingOS;
        this.$ac_game = $("#" + this.id);
        this.settings = new Settings(this);
        this.menu = new AcGameMenu(this);
        this.menu.hide();
        this.playground = new AcGamePlayground(this);   
        this.playground.hide();
    }

    start(){

    }
    
}

