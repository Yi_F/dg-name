class Settings{


    constructor(root){
        this.root = root;
        this.platform;
        if(this.root.AcWingOS) this.platform = "ACAPP";
        else this.platform = "WEB";
        this.photo = "";
        this.username = "";

        this.$settings = $(`
<div class="ac-game-settings">
    <div class="ac-game-settings-login ac-game-settings-field">
        <div class="ac-game-settings-field-title">
        登录
        </div>
        <div class="ac-game-settings-field-username">
            <div class="ac-game-settings-item">
                <input type="text" placeholder="用户名">
            </div>
        </div>
        <br/>
        <div class="ac-game-settings-field-password ">
            <div class="ac-game-settings-item">
                <input type="password" placeholder="密码">
            </div>
        </div>
        <br/>
        <div class="ac-game-settings-field-submit">
            <div class="ac-game-settings-item">
                <button>登录</button>
            </div>
        </div>
        <div>
            <div class="ac-game-settings-field-error-messages">
               错误消息
            </div>
            <div class="ac-game-settings-field-options">
                注册
            </div>
        </div>
        <br/>
        <div class="ac-game-settings-acwing">
            <img width="30" src="https://app165.acapp.acwing.com.cn/static/image/settings/acwing_logo.png">
            <div>
                AcWing一键登录
            </div>
        </div>
    </div>



    <div class="ac-game-settings-register ac-game-settings-field">
        <div class="ac-game-settings-field-title">
        注册
        </div>
        <div class="ac-game-settings-field-username">
            <div class="ac-game-settings-item">
                <input type="text" placeholder="用户名">
            </div>
        </div>
        <br/>
        <div class="ac-game-settings-field-password ">
            <div class="ac-game-settings-item">
                <input type="password" placeholder="密码">
            </div>
        </div>
        <br/>

        <div class="ac-game-settings-field-comfirm-password ">
            <div class="ac-game-settings-item">
                <input type="password" placeholder="确认密码">
            </div>
        </div>

        <br/>
        <div class="ac-game-settings-field-submit">
            <div class="ac-game-settings-item">
                <button>注册</button>
            </div>
        </div>
        <div>
            <div class="ac-game-settings-field-error-messages">
               错误消息
            </div>
            <div class="ac-game-settings-field-options">
                登录
            </div>
        </div>
        <br/>
    </div>


</div>

            `);

        this.root.$ac_game.append(this.$settings);

       this.$login = this.$settings.find(".ac-game-settings-login");
       this.$login_username = this.$login.find(".ac-game-settings-field-username input");
       this.$login_pd = this.$login.find(".ac-game-settings-field-password input");
       this.$login_button = this.$login.find(".ac-game-settings-field-submit button");
       this.$login_error = this.$login.find(".ac-game-settings-field-error-messages");
       this.$login_option = this.$login.find(".ac-game-settings-field-options");
        this.$login_acwing = this.$login.find(".ac-game-settings-acwing");

       this.$register = this.$settings.find(".ac-game-settings-register");
       this.$register_username = this.$register.find(".ac-game-settings-field-username input");
       this.$register_pd = this.$register.find(".ac-game-settings-field-password input");
        this.$register_cd_pd = this.$register.find(".ac-game-settings-field-comfirm-password input");
        this.$register_error = this.$register.find(".ac-game-settings-field-error-messages");
        this.$register_option = this.$register.find(".ac-game-settings-field-options");
        this.$register_button = this.$register.find(".ac-game-settings-field-submit button");



        console.log(this);

        this.start();
    }

    start(){
        this.getinfo();
        this.add_listening_event();
        this.login();
    }

    add_listening_event(){
       
        this.add_listening_register();
        this.add_listening_login();
        this.add_listening_logout();
    }

    add_listening_logout(){
        let outer = this;
    }
    
    logout_to_remote(){
        
        $.ajax({
            
            url:'https://app2156.acapp.acwing.com.cn/settings/aclogout/',
            type: 'GET',
            data:{

            },
            success: function(resp){
                console.log(resp);
                location.reload();
            }

        });

    }
    add_listening_login(){
        let outer = this;
        this.$login_option.click(function(){
           outer.register();
        });

        this.$login_button.click(function(){

            outer.login_to_remote();

        });

        this.$login_acwing.click(function(){

            outer.acwing_web_login();

        });


    }

    login(){
        
        this.$login.show();
        this.$register.hide();

    }

    login_to_remote(){
        let outer = this;
        let username = this.$login_username.val();
        let password = this.$login_pd.val();
        this.$login_error.empty();
        $.ajax({

            'url':'https://app2156.acapp.acwing.com.cn/settings/aclogin/',
            'type': 'GET',
            'data': {
                
                'username': username,
                'password': password,

            },
            'success': function(resp){
                console.log(resp);

                if(resp.result === 'success'){
                    location.reload();
                }else{
                    outer.$login_error.html(resp.result);
                }


            }
            

        });
    }

    acwing_web_login(){


        $.ajax({

            url:'https://app2156.acapp.acwing.com.cn/settings/acwing/web/apply_code/',
            type: 'GET',
            data:{},
            success: function(resp){
                if(resp.result === 'success'){
                    window.location.replace(resp.apply_code_url);
                }

            }

        });


    }

    add_listening_register(){
        let outer = this; 
        this.$register_option.click(function(){
            outer.login();

        });
        this.$register_button.click(function(){
            outer.register_to_remote();

        });

    }

    register(){
        this.$login.hide();
        this.$register.show();
    }

    register_to_remote(){
        let outer = this;
        let username = this.$register_username.val();
        let password = this.$register_pd.val();
        let comfirm_password = this.$register_cd_pd.val();
        this.$register_error.empty();
        
        $.ajax({

            url:'https://app2156.acapp.acwing.com.cn/settings/acregister/',
            type: 'GET',
            data:{
                'username': username,
                'password': password,
                'comfirm_password': comfirm_password,
            },
            success: function(resp){
                if(resp.result === 'success'){
                    
                    location.reload();

                }else{
                    outer.$register_error.html(resp.result);
                }

            }

        });

    }

    getinfo(){
        let outer = this;

        $.ajax({
            url: "https://app2156.acapp.acwing.com.cn/settings/getinfo/",
            type: "GET",
            data:{
                "platform": this.platform,
            },
            success: function(resp){
                console.log(resp);
                if(resp.result === "success"){
                    outer.photo = resp.photo;
                    outer.username = resp.photo;
                    outer.hide();
                    outer.root.menu.show();

                }else{

                    outer.login();

                }
            }

        });


    }


    show(){
        
        this.$settings.show();

    }

    hide(){
        this.$settings.hide();

    }

}
