class Particle extends AcGameObject{

    constructor(game_map,x,y,radius,color,vx,vy,speed,move_length){
        super();
        this.game_map = game_map;
        this.ctx = this.game_map.ctx;
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.color = color;
        this.vx = vx;
        this.vy = vy;
        this.speed = speed;
        this.friction = 0.9;
        this.move_length = move_length;
        this.eps = 0.1;
    }

    start(){
        


    }

    update(){
        if(this.move_length < this.eps || this.speed < this.eps){
            this.destroy();
            return ;
        }
    
        let t = this.speed * this.timedetla / 1000;
        this.x += t * this.vx;
        this.y += t*this.vy;
        this.speed *= this.friction;
        this.move_length -= t;
        this.render();
    }

    render(){
        this.ctx.beginPath();
        this.ctx.fillStyle = this.color;
        this.ctx.arc(this.x,this.y,this.radius,0,2*Math.PI,false);
        this.ctx.fill();
    }

    on_destroy(){

    }


}
