let AC_GAME_OBJECTS = [];

class AcGameObject{

    constructor(){

        AC_GAME_OBJECTS.push(this);
        this.timedetla = 0;
        this.is_called_start = false;
    }

    start(){



    }


    update(){



    }

    on_destroy(){


    }

    destroy(){
        this.on_destroy();

        for(let i=0;i < AC_GAME_OBJECTS.length;i++){
            if(AC_GAME_OBJECTS[i] === this){
                AC_GAME_OBJECTS.splice(i,1);
            }

        }

    }

}

let last_time = 0;

let AC_GAME_ANIMATION = function (timestamp) {
    
    for(let i=0;i<AC_GAME_OBJECTS.length;i++){
        let x = AC_GAME_OBJECTS[i];
        if(! x.is_called_start){
            x.start();
            x.is_called_start = true;
        }else {
            x.timedetla = timestamp - last_time;

            x.update();
        }

    }
    last_time = timestamp;
    requestAnimationFrame(AC_GAME_ANIMATION);

}
requestAnimationFrame(AC_GAME_ANIMATION);



