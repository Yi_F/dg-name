class AcGamePlayground{

    constructor(root){
        this.root = root;
        this.$playground = $(`
            <div class="ac-game-playground"></div>
            `);

        this.$playground.hide();
        this.root.$ac_game.append(this.$playground);
        
    }


    start(){
        this.add_listening_event();
    }

    add_listening_event(){


    }


    show(){
        this.width = this.$playground.width();
        this.height = this.$playground.height();
        this.game_map = new AcGameMap(this);
        this.start();
        this.$playground.show();
    }

    hide(){
        this.$playground.hide();
    }

}
