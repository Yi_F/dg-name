class AcGameMap extends AcGameObject{
    
    constructor(playground){
        super();
        this.playground = playground;
        this.$canvas = $(`<canvas></canvas>`);
        this.ctx = this.$canvas[0].getContext("2d");
        this.playground.$playground.append(this.$canvas);
        this.width = this.playground.width;
        this.height = this.playground.height;
        this.$canvas[0].width = this.width;
        this.$canvas[0].height = this.height;
        this.players = [];
        console.log("---");;
        this.self_player = new AcGamePlayer(this,this.width / 2,this.height / 2 ,this.height * 0.05 , this.height * 0.30 , true);
        this.players.push(this.self_player);
        console.log(this.self_player);
        this.balls = [];
    }

    start(){
        this.create_some_players();
    }

    update(){
        this.render();
        this.check_collision();
    }

    destory(){


    }

    render(){
       this.ctx.fillStyle = "rgb(0,0,0,0.2)";
       this.ctx.fillRect(0,0,this.width,this.height);
    }


    create_some_players(){

        let colors = ["blue","red","green","#e1aa15","#b1c542"];
        let n = 5;
        let one = this.self_player;

        for(let i = 0;i<n;i++){
            this.players.push(new AcGamePlayer(this,one.x,one.y,one.radius,one.speed,false,colors[Math.floor(Math.random() * colors.length)]));

        }

    }

    check_vaild(a){
        if(a.x<0 || a.y < 0 || a.x > this.width || a.y > this.height){
            
            return false;

        }
        else{
            return true;
        }
    }

    check_collision(){
        console.log(this.balls.length);
       const st = new Array(9).fill(false);
       for(let i=0;i<this.balls.length;i++){
           let a = this.balls[i];
           if(st[i] || !this.check_vaild(a)){
               st[i] = true;
               continue;
           }
           let t = false;
           for(let j = i+1;j<this.balls.length;j++){
                let b = this.balls[j];
               if(st[j] || !this.check_vaild(b)){
                    st[j] = true;
                   continue;
               }
                if(this.is_collision(a,b)){
                    st[i] = true;
                    st[j] = true;
                    t = true;
                }

           }
           if(t)  continue;
           
           for(let j = 0;j < this.players.length;j++){
                let b =this.players[j];
                if(a.player != b && this.is_collision(a,b)){
                    a.attackPlayer(b);
                    st[i] = true;
                }
           }

           let nums = [];
           for(let i =0;i<this.balls.length;i++){
                let a = this.balls[i];
                if(st[i])   a.destroy();
                else nums.push(a);
           }
           this.balls = nums;
       }


    }

    is_collision(a,b){

        if(!a.x || !a.y || ! a.radius || ! b.x || !b.y || !b.radius)    return ;
        return this.getDist(a,b)<(a.radius + b.radius);

    }

    getDist(a,b){
        return Math.sqrt((a.x-b.x)*(a.x-b.x) + (a.y - b.y)*(a.y-b.y));
    }


}
