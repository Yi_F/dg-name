class AcGameFireball extends AcGameObject{

    constructor(game_map,player,x,y,radius,vx,vy,speed,move_len,dange,color){
        super();
        this.game_map = game_map;
        this.ctx = this.game_map.ctx;
        this.player = player;
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.vx = vx;
        this.vy = vy;
        this.move_length = move_len;
        this.dange = dange;
        this.speed = speed;
        this.color = color;
        this.eps = 0.1;
    }


    start(){


    }

    update(){
        if(this.move_length < this.eps){
            this.destroy();
            let balls = this.game_map.balls;
            for(let i=0;i<this.game_map.balls.length;i++){
                if(this === balls[i]){
                    balls.splice(i,1);
                    break;
                }
            }
            return  ;
        }else{
            let t = this.speed * this.timedetla / 1000;
            this.x += t * this.vx;
            this.y += t * this.vy;
            this.move_length -= t;
        }

        this.render();
    }

    destroy(){
        let balls = this.player.shoted_balls;
        for(let i = 0;i < balls.length;i++){

            if(balls[i] === this){
                balls.splice(i,1);
                break;
            }

        }
        super.destroy();

    }

    attackPlayer(player){

        let t = Math.atan2(player.y-this.y,player.x-this.x);
        let dx = Math.cos(t);
        let dy = Math.sin(t);

       player.attacked(dx,dy,this.speed * 0.75,this.dange,this.dange/player.radius * this.game_map.height / 2);
       // console.log(this.game_map.height);
        //player.attacked(dx,dy,this.speed * 0.75,this.dange,this.game_map.height/1000);


    }

    

    render(){
        
        this.ctx.beginPath();
        this.ctx.fillStyle = this.color;
        this.ctx.arc(this.x,this.y,this.radius,0,2 * Math.PI,false);
        this.ctx.fill();
    }

}
